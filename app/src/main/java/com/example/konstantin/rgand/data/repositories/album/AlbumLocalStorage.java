package com.example.konstantin.rgand.data.repositories.album;

import com.example.konstantin.rgand.data.db.RoomDb;
import com.example.konstantin.rgand.data.db.entity.AlbumDb;
import com.example.konstantin.rgand.data.db.entity.AlbumImageDb;
import com.example.konstantin.rgand.data.db.utils.AlbumTransferModel;
import com.example.konstantin.rgand.data.db.utils.ModelMapper;
import com.example.konstantin.rgand.net.model.Album;
import com.example.konstantin.rgand.net.model.Image;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class AlbumLocalStorage implements AlbumRepository {

    private final RoomDb mRoomDb;
    private final ModelMapper<Image, AlbumImageDb> mAlbumImageMapper;
    private final ModelMapper<Album, AlbumDb> mAlbumMapper;

    @Inject
    public AlbumLocalStorage(RoomDb roomDb, ModelMapper<Image, AlbumImageDb> albumImageMapper,
                             ModelMapper<Album, AlbumDb> albumMapper) {
        mRoomDb = roomDb;
        mAlbumMapper = albumMapper;
        mAlbumImageMapper = albumImageMapper;
    }

    @Override
    public void saveArtistAlbums(List<Album> albums) {
        Observable.fromIterable(albums)
                .observeOn(Schedulers.computation())
                .map(new Function<Album, AlbumTransferModel>() {
                    @Override
                    public AlbumTransferModel apply(Album album) throws Exception {
                        final AlbumTransferModel transferModel = new AlbumTransferModel();
                        transferModel.setAlbumDb(mAlbumMapper.mapIntoDbModel(album));

                        final List<AlbumImageDb> imageDbs = new ArrayList<>(album.getImage().size());
                        AlbumImageDb tmp;
                        for (Image image : album.getImage()) {
                            tmp = mAlbumImageMapper.mapIntoDbModel(image);
                            tmp.setAlbumId(album.getMbid());
                            imageDbs.add(tmp);
                        }
                        transferModel.setAlbumImageDbs(imageDbs);

                        return transferModel;
                    }
                })
                .toList()
                .subscribe(new Consumer<List<AlbumTransferModel>>() {
                    @Override
                    public void accept(List<AlbumTransferModel> albumTransferModels) throws Exception {
                        final List<AlbumDb> albumDbs = new ArrayList<>(albumTransferModels.size());
                        final List<AlbumImageDb> imageDbs = new ArrayList<>();
                        for (AlbumTransferModel albumTransferModel : albumTransferModels) {
                            albumDbs.add(albumTransferModel.getAlbumDb());
                            imageDbs.addAll(albumTransferModel.getAlbumImageDbs());
                        }
                        mRoomDb.albumDao().insertArtistAlbums(albumDbs);
                        mRoomDb.albumImageDao().saveAlbumImages(imageDbs);
                    }
                });
    }

    @Override
    public Single<List<Album>> getArtistsAlbums(final String artistId) {
        return mRoomDb.albumDao().getAllArtistsAlbums(artistId)
                .toObservable()
                .flatMapIterable(new Function<List<AlbumDb>, List<AlbumDb>>() {
                    @Override
                    public List<AlbumDb> apply(List<AlbumDb> albumDbEntities) throws Exception {
                        if (albumDbEntities.isEmpty()) {
                            throw new EmptyStackException();
                        }
                        return albumDbEntities;
                    }
                })
                .map(new Function<AlbumDb, Album>() {
                    @Override
                    public Album apply(AlbumDb albumDbEntity) throws Exception {
                        final Album album = mAlbumMapper.mapIntoLogicModel(albumDbEntity);
                        final List<AlbumImageDb> imageDbs = mRoomDb.albumImageDao()
                                .getAllImagesForAlbum(albumDbEntity.getId());
                        final List<Image> images = new ArrayList<>(imageDbs.size());
                        for (AlbumImageDb imageDb : imageDbs) {
                            images.add(mAlbumImageMapper.mapIntoLogicModel(imageDb));
                        }
                        album.setImage(images);
                        return album;
                    }
                })
                .toList();
    }

}
