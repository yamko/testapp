package com.example.konstantin.rgand.ui.artist;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.konstantin.rgand.R;
import com.example.konstantin.rgand.net.model.Album;
import com.example.konstantin.rgand.ui.base.BaseRecyclerAdapter;
import com.example.konstantin.rgand.ui.base.KnifeViewHolder;
import com.example.konstantin.rgand.utils.ImageUtils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import butterknife.BindView;

public class AlbumsAdapter extends BaseRecyclerAdapter<Album> {

    public AlbumsAdapter(Context context) {
        super(context);
    }

    @Override
    public KnifeViewHolder<Album> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AlbumVH(mLayoutInflater.inflate(R.layout.item_album, parent, false));
    }

    static class AlbumVH extends KnifeViewHolder<Album> {

        @BindView(R.id.item_album_image) ImageView mAlbumImage;
        @BindView(R.id.item_album_name) TextView mAlbumName;
        @BindView(R.id.item_album_listeners) TextView mAlbumListeners;

        private final DecimalFormat mDecimalFormat;

        AlbumVH(View itemView) {
            super(itemView);
            mDecimalFormat =(DecimalFormat) NumberFormat.getInstance(Locale.US);
            final DecimalFormatSymbols symbols = mDecimalFormat.getDecimalFormatSymbols();
            symbols.setGroupingSeparator(' ');
            mDecimalFormat.setDecimalFormatSymbols(symbols);
        }

        @Override
        public void fillData(Album object) {
            mAlbumName.setText(object.getName());
            mAlbumListeners.setText(mDecimalFormat.format(object.getPlaycount()));
            ImageUtils.loadImage(itemView.getContext(),
                    ImageUtils.getImageUrl(ImageUtils.LARGE, object.getImage()), mAlbumImage);
        }
    }

}
