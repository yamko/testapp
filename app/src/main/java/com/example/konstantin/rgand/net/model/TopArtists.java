
package com.example.konstantin.rgand.net.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TopArtists {

    @SerializedName("artist")
    private List<Artist> artist;
//    @SerializedName("@attr")
//    private ArtistAttr attr;

    public List<Artist> getArtist() {
        return artist;
    }

//    public ArtistAttr getAttr() {
//        return attr;
//    }

}
