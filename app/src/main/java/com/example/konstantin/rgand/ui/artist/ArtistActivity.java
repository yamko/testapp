package com.example.konstantin.rgand.ui.artist;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;

import com.example.konstantin.rgand.R;
import com.example.konstantin.rgand.core.RgApp;
import com.example.konstantin.rgand.di.AlbumsComponent;
import com.example.konstantin.rgand.di.AlbumsModule;
import com.example.konstantin.rgand.net.model.Album;
import com.example.konstantin.rgand.ui.base.BaseActivity;
import com.example.konstantin.rgand.utils.ImageUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class ArtistActivity extends BaseActivity implements ArtistContract.ArtistView {

    public static final String ARTIST = "artist";
    public static final String ARTIST_ID = "artist_id";
    public static final String IMAGE = "image";

    @BindView(R.id.artist_image) ImageView mArtistImage;
    @BindView(R.id.artist_albums) RecyclerView mArtistAlbums;
    @BindView(R.id.artist_toolbar) Toolbar mToolbar;

    @Inject ArtistContract.ArtistPresenter mAlbumsPresenter;

    private AlbumsAdapter mAlbumsAdapter;
    private AlbumsComponent mAlbumsComponent;

    @LayoutRes
    @Override
    protected int getContentView() {
        return R.layout.activity_artist;
    }

    public static void start(Activity context, String artistName, String imageUrl, View imageView,
                             String artistId) {
        final Intent starter = new Intent(context, ArtistActivity.class);
        starter.putExtra(ARTIST, artistName);
        starter.putExtra(IMAGE, imageUrl);
        starter.putExtra(ARTIST_ID, artistId);
        final ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(context, imageView, "artist_image");
        context.startActivity(starter, options.toBundle());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAlbumsComponent = ((RgApp) getApplication()).getAppComponent()
                .provideAlbumsComponent(new AlbumsModule());
        mAlbumsComponent.inject(this);

        mAlbumsAdapter = new AlbumsAdapter(this);
        mArtistAlbums.setAdapter(mAlbumsAdapter);
        mArtistAlbums.setLayoutManager(new LinearLayoutManager(this));

        setUpToolbar();
        setArtistImageHeight();
        loadArtistImage(getIntent().getStringExtra(IMAGE));
    }

    private void setUpToolbar(){
        setSupportActionBar(mToolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
        }
        setTitle(getIntent().getStringExtra(ARTIST));
    }

    private void setArtistImageHeight() {
        final Display display = getWindowManager().getDefaultDisplay();
        final Point point = new Point();
        display.getSize(point);
        mArtistImage.getLayoutParams().height = point.y / 2;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAlbumsPresenter.bindView(this);
        mAlbumsPresenter.loadAlbums(getIntent().getStringExtra(ARTIST));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAlbumsPresenter.unbindView();
    }

    @Override
    public void showAlbums(List<Album> albums) {
        mAlbumsAdapter.setItems(albums);
    }

    @Override
    public void loadArtistImage(String url) {
        ImageUtils.loadImage(this, url, mArtistImage);
    }
}
