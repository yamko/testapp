package com.example.konstantin.rgand.data.repositories.country;

import java.util.List;

import io.reactivex.Observable;

public interface CountriesRepository {

    Observable<List<String>> loadCountries();

}
