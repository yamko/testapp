package com.example.konstantin.rgand.data.repositories.album;

import com.example.konstantin.rgand.net.RetrofitClient;
import com.example.konstantin.rgand.net.model.Album;
import com.example.konstantin.rgand.net.model.TopAlbumsResponse;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.functions.Function;

public class AlbumRemoteStorage implements AlbumRepository {

    private final RetrofitClient mRetrofitClient;

    @Inject
    public AlbumRemoteStorage(RetrofitClient retrofitClient) {
        mRetrofitClient = retrofitClient;
    }

    @Override
    public void saveArtistAlbums(List<Album> albums) {

    }

    @Override
    public Single<List<Album>> getArtistsAlbums(String artist) {
        return mRetrofitClient.getService().getArtistsAlbums(artist)
                .map(new Function<TopAlbumsResponse, List<Album>>() {
                    @Override
                    public List<Album> apply(TopAlbumsResponse topAlbumsResponse) throws Exception {
                        return topAlbumsResponse.getTopAlbums().getAlbum();
                    }
                });
    }
}
