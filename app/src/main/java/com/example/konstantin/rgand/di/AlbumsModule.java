package com.example.konstantin.rgand.di;

import com.example.konstantin.rgand.data.repositories.album.AlbumRepository;
import com.example.konstantin.rgand.di.qualifier.Repository;
import com.example.konstantin.rgand.di.scope.PerActivity;
import com.example.konstantin.rgand.ui.artist.ArtistContract;
import com.example.konstantin.rgand.ui.artist.ArtistPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class AlbumsModule {

    @PerActivity
    @Provides
    ArtistContract.ArtistPresenter provideAlbumsPresenter(@Repository AlbumRepository albumsRepository){
        return new ArtistPresenterImpl(albumsRepository);
    }
}
