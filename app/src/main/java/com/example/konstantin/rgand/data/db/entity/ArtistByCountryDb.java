package com.example.konstantin.rgand.data.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity(foreignKeys = @ForeignKey(entity = ArtistDb.class,
        parentColumns = "artist_id", childColumns = "artist_id",
        onDelete = ForeignKey.CASCADE))
public class ArtistByCountryDb {

    public ArtistByCountryDb(String artistId, String country) {
        mArtistId = artistId;
        mCountry = country;
    }

    @PrimaryKey(autoGenerate = true)
    private int mId;

    @ColumnInfo(name = "artist_id")
    private String mArtistId;

    @ColumnInfo(name = "country_name")
    private String mCountry;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getArtistId() {
        return mArtistId;
    }

    public void setArtistId(String artistId) {
        mArtistId = artistId;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }
}
