
package com.example.konstantin.rgand.net.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Album {

    @SerializedName("name")
    private String name;
    @SerializedName("playcount")
    private Integer playcount;
    @SerializedName("mbid")
    private String mbid;
    @SerializedName("url")
    private String url;
    @SerializedName("artist")
    private Artist artist;
    @SerializedName("image")
    private List<Image> image;

    public String getName() {
        return name;
    }

    public Integer getPlaycount() {
        return playcount;
    }

    public String getMbid() {
        return mbid;
    }

    public String getUrl() {
        return url;
    }

    public Artist getArtist() {
        return artist;
    }

    public List<Image> getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPlaycount(Integer playcount) {
        this.playcount = playcount;
    }

    public void setMbid(String mbid) {
        this.mbid = mbid;
    }

    public void setImage(List<Image> image) {
        this.image = image;
    }
}
