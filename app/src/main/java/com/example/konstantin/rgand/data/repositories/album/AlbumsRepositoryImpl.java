package com.example.konstantin.rgand.data.repositories.album;

import com.example.konstantin.rgand.net.model.Album;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;

public class AlbumsRepositoryImpl implements AlbumRepository {

    private AlbumRepository mLocal;
    private AlbumRepository mRemote;

    @Inject
    public AlbumsRepositoryImpl(AlbumRepository remote, AlbumRepository local) {
        mRemote = remote;
        mLocal = local;
    }

    @Override
    public void saveArtistAlbums(List<Album> albums) {

    }

    @Override
    public Single<List<Album>> getArtistsAlbums(final String artistId) {
        return mLocal.getArtistsAlbums(artistId)
                .onErrorResumeNext(new Function<Throwable, SingleSource<? extends List<Album>>>() {
                    @Override
                    public SingleSource<? extends List<Album>> apply(Throwable throwable) throws Exception {
                        return mRemote.getArtistsAlbums(artistId).
                                map(new Function<List<Album>, List<Album>>() {
                                    @Override
                                    public List<Album> apply(List<Album> albums) throws Exception {
                                        mLocal.saveArtistAlbums(albums);
                                        return albums;
                                    }
                                });
                    }
                });
    }
}
