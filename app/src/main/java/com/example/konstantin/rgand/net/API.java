package com.example.konstantin.rgand.net;

import com.example.konstantin.rgand.BuildConfig;
import com.example.konstantin.rgand.net.model.TopAlbumsResponse;
import com.example.konstantin.rgand.net.model.TopArtistsResponse;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface API {

    @GET("?method=geo.gettopartists&api_key=" + BuildConfig.apiKey + "&format=json")
    Single<TopArtistsResponse> getTopArtist(@Query("country") String country);

    @GET("?method=artist.gettopalbums&api_key=" + BuildConfig.apiKey + "&format=json")
    Single<TopAlbumsResponse> getArtistsAlbums(@Query("artist") String artist);

}
