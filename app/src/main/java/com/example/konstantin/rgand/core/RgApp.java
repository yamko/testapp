package com.example.konstantin.rgand.core;

import android.app.Application;

import com.example.konstantin.rgand.di.AppComponent;
import com.example.konstantin.rgand.di.AppModule;
import com.example.konstantin.rgand.di.DaggerAppComponent;

public class RgApp extends Application {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

}
