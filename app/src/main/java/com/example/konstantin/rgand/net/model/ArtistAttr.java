
package com.example.konstantin.rgand.net.model;

import com.google.gson.annotations.SerializedName;

public class ArtistAttr {

    @SerializedName("country")
    private String country;
    @SerializedName("page")
    private String page;
    @SerializedName("perPage")
    private String perPage;
    @SerializedName("totalPages")
    private String totalPages;
    @SerializedName("total")
    private String total;

    public String getCountry() {
        return country;
    }

    public String getPage() {
        return page;
    }

    public String getPerPage() {
        return perPage;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public String getTotal() {
        return total;
    }

}
