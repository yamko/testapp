package com.example.konstantin.rgand.data.repositories.artist;

import com.example.konstantin.rgand.net.RetrofitClient;
import com.example.konstantin.rgand.net.model.Artist;
import com.example.konstantin.rgand.net.model.TopArtistsResponse;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.functions.Function;

public class ArtistRemoteStorage implements ArtistsRepository {

    private final RetrofitClient mRetrofitClient;

    @Inject
    public ArtistRemoteStorage(RetrofitClient retrofitClient) {
        mRetrofitClient = retrofitClient;
    }

    @Override
    public void saveArtists(List<Artist> artists, String country) {

    }

    @Override
    public Single<List<Artist>> getArtists(String country) {
        return mRetrofitClient.getService().getTopArtist(country)
                .map(new Function<TopArtistsResponse, List<Artist>>() {
                    @Override
                    public List<Artist> apply(TopArtistsResponse topArtistsResponse) throws Exception {
                        return topArtistsResponse.getTopArtists().getArtist();
                    }
                });
    }
}
