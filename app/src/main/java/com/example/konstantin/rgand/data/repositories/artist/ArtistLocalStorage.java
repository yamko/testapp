package com.example.konstantin.rgand.data.repositories.artist;

import com.example.konstantin.rgand.data.db.RoomDb;
import com.example.konstantin.rgand.data.db.entity.ArtistByCountryDb;
import com.example.konstantin.rgand.data.db.entity.ArtistDb;
import com.example.konstantin.rgand.data.db.entity.ArtistImageDb;
import com.example.konstantin.rgand.data.db.utils.ArtistTransferModel;
import com.example.konstantin.rgand.data.db.utils.ModelMapper;
import com.example.konstantin.rgand.net.model.Artist;
import com.example.konstantin.rgand.net.model.Image;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class ArtistLocalStorage implements ArtistsRepository {

    private final RoomDb mRoomDb;
    private final ModelMapper<Image, ArtistImageDb> mArtistImageMapper;
    private final ModelMapper<Artist, ArtistDb> mArtistMapper;

    @Inject
    public ArtistLocalStorage(RoomDb roomDb, ModelMapper<Image, ArtistImageDb> artistImageMapper,
                              ModelMapper<Artist, ArtistDb> artistMapper) {
        mRoomDb = roomDb;
        mArtistImageMapper = artistImageMapper;
        mArtistMapper = artistMapper;
    }

    @Override
    public void saveArtists(List<Artist> artists, final String country) {
        Observable.fromIterable(artists)
                .observeOn(Schedulers.computation())
                .map(new Function<Artist, ArtistTransferModel>() {
                    @Override
                    public ArtistTransferModel apply(Artist artist) throws Exception {
                        final ArtistTransferModel transferModel = new ArtistTransferModel();
                        transferModel.setArtistDb(mArtistMapper.mapIntoDbModel(artist));

                        final List<ArtistImageDb> imageDbs = new ArrayList<>(artist.getImage().size());
                        ArtistImageDb tmp;
                        for (Image image : artist.getImage()) {
                            tmp = mArtistImageMapper.mapIntoDbModel(image);
                            tmp.setArtistId(artist.getMbid());
                            imageDbs.add(tmp);
                        }
                        transferModel.setArtistImageDbs(imageDbs);

                        return transferModel;
                    }
                })
                .toList()
                .subscribe(new Consumer<List<ArtistTransferModel>>() {
                    @Override
                    public void accept(List<ArtistTransferModel> artistTransferModels) throws Exception {
                        final List<ArtistDb> artistDbs = new ArrayList<>(artistTransferModels.size());
                        final List<ArtistImageDb> imageDbs = new ArrayList<>();
                        for (ArtistTransferModel artistTransferModel : artistTransferModels) {
                            artistDbs.add(artistTransferModel.getArtistDb());
                            imageDbs.addAll(artistTransferModel.getArtistImageDbs());
                        }
                        final List<ArtistByCountryDb> artistByCountryDbs = new ArrayList<>(artistDbs.size());
                        for (ArtistDb artistDb : artistDbs) {
                            artistByCountryDbs.add(new ArtistByCountryDb(artistDb.getId(), country));
                        }

                        mRoomDb.artistDao().insertArtist(artistDbs);
                        mRoomDb.artistImageDao().saveArtistsImages(imageDbs);
                        mRoomDb.artistInCountryDao().insert(artistByCountryDbs);
                    }
                });
    }

    @Override
    public Single<List<Artist>> getArtists(final String country) {
        return mRoomDb.artistDao().getArtistsByCountry(country)
                .toObservable()
                .flatMapIterable(new Function<List<ArtistDb>, List<ArtistDb>>() {
                    @Override
                    public List<ArtistDb> apply(List<ArtistDb> artistDbs) throws Exception {
                        if (artistDbs.isEmpty()) {
                            throw new EmptyStackException();
                        }
                        return artistDbs;
                    }
                })
                .map(new Function<ArtistDb, Artist>() {
                    @Override
                    public Artist apply(ArtistDb artistDb) throws Exception {
                        final Artist artist = mArtistMapper.mapIntoLogicModel(artistDb);
                        final List<ArtistImageDb> imageDbs = mRoomDb.artistImageDao()
                                .getAllImagesForAtrist(artistDb.getId());
                        final List<Image> images = new ArrayList<>(imageDbs.size());
                        for (ArtistImageDb imageDb : imageDbs) {
                            images.add(mArtistImageMapper.mapIntoLogicModel(imageDb));
                        }
                        artist.setImage(images);
                        return artist;
                    }
                })
                .toList();
    }

}
