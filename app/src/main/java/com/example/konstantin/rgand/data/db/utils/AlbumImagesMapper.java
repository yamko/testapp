package com.example.konstantin.rgand.data.db.utils;

import com.example.konstantin.rgand.data.db.entity.AlbumImageDb;
import com.example.konstantin.rgand.net.model.Image;

public class AlbumImagesMapper implements ModelMapper<Image, AlbumImageDb> {

    public AlbumImagesMapper() {
    }

    @Override
    public AlbumImageDb mapIntoDbModel(Image item) {
        final AlbumImageDb imageDb = new AlbumImageDb();
        imageDb.setSize(item.getSize());
        imageDb.setUrl(item.getText());
        return imageDb;
    }

    @Override
    public Image mapIntoLogicModel(AlbumImageDb item) {
        final Image image = new Image();
        image.setSize(item.getSize());
        image.setText(item.getUrl());
        return image;
    }
}
