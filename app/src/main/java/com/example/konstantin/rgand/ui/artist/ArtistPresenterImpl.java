package com.example.konstantin.rgand.ui.artist;

import com.example.konstantin.rgand.data.repositories.album.AlbumRepository;
import com.example.konstantin.rgand.net.model.Album;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ArtistPresenterImpl extends ArtistContract.ArtistPresenter {

    private final AlbumRepository mAlbumsRepository;

    @Inject
    public ArtistPresenterImpl(AlbumRepository albumsRepository) {
        mAlbumsRepository = albumsRepository;
    }

    @Override
    void loadAlbums(String artistName) {
        mAlbumsRepository.getArtistsAlbums(artistName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Consumer<List<Album>>() {
                            @Override
                            public void accept(List<Album> albums) throws Exception {
                                mView.showAlbums(albums);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {

                            }
                        }
                );
    }

}
