package com.example.konstantin.rgand.ui.opening;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.konstantin.rgand.R;
import com.example.konstantin.rgand.ui.base.BaseSpinnerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Konstantin on 8/27/2017.
 */

public class CountriesAdapter extends BaseSpinnerAdapter<String> {

    public CountriesAdapter(Context context) {
        super(context);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        CountryViewHolder countryViewHolder;
        if (convertView != null) {
            if (convertView.getTag() != null) {
                countryViewHolder = (CountryViewHolder) convertView.getTag();
            } else {
                countryViewHolder = new CountryViewHolder(convertView);
                convertView.setTag(countryViewHolder);
            }
        } else {
            convertView = mLayoutInflater.inflate(R.layout.item_county, parent, false);
            countryViewHolder = new CountryViewHolder(convertView);
            convertView.setTag(countryViewHolder);
        }
        countryViewHolder.fillData(mItems.get(position));
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CountryViewHolder countryViewHolder;
        if (convertView != null) {
            if (convertView.getTag() != null) {
                countryViewHolder = (CountryViewHolder) convertView.getTag();
            } else {
                countryViewHolder = new CountryViewHolder(convertView);
                convertView.setTag(countryViewHolder);
            }
        } else {
            convertView = mLayoutInflater.inflate(R.layout.item_selected_country, parent, false);
            countryViewHolder = new CountryViewHolder(convertView);
            convertView.setTag(countryViewHolder);
        }
        countryViewHolder.fillData(mItems.get(position));
        return convertView;
    }

    static final class CountryViewHolder {

        @BindView(R.id.item_country_name) TextView mTextView;

        public CountryViewHolder(View rootView) {
            ButterKnife.bind(this, rootView);
        }

        public void fillData(String data) {
            mTextView.setText(data);
        }
    }
}
