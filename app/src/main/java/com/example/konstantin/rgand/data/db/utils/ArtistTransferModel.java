package com.example.konstantin.rgand.data.db.utils;

import com.example.konstantin.rgand.data.db.entity.ArtistDb;
import com.example.konstantin.rgand.data.db.entity.ArtistImageDb;

import java.util.List;

public class ArtistTransferModel {

    private ArtistDb mArtistDb;

    private List<ArtistImageDb> mArtistImageDbs;

    public ArtistDb getArtistDb() {
        return mArtistDb;
    }

    public void setArtistDb(ArtistDb artistDb) {
        mArtistDb = artistDb;
    }

    public List<ArtistImageDb> getArtistImageDbs() {
        return mArtistImageDbs;
    }

    public void setArtistImageDbs(List<ArtistImageDb> albumImageDbs) {
        mArtistImageDbs = albumImageDbs;
    }

}
