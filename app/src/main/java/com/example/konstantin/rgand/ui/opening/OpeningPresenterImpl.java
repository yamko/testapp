package com.example.konstantin.rgand.ui.opening;

import com.example.konstantin.rgand.data.repositories.artist.ArtistsRepository;
import com.example.konstantin.rgand.data.repositories.country.CountriesRepository;
import com.example.konstantin.rgand.di.qualifier.Repository;
import com.example.konstantin.rgand.net.model.Artist;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class OpeningPresenterImpl extends OpeningContract.OpeningPresenter {

    private final ArtistsRepository mArtistsRepository;
    private final CountriesRepository mCountriesRepository;

    @Inject
    public OpeningPresenterImpl(@Repository ArtistsRepository artistsRepository, CountriesRepository countriesRepository) {
        mArtistsRepository = artistsRepository;
        mCountriesRepository = countriesRepository;
        init();
    }

    private void init() {
        mCountriesRepository.loadCountries()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Consumer<List<String>>() {
                            @Override
                            public void accept(List<String> strings) throws Exception {
                                mView.showLoadedCountries(strings);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {

                            }
                        });
    }

    @Override
    public void loadArtistsByCountry(String country) {
        mArtistsRepository.getArtists(country)
                .subscribeOn(Schedulers.io())
                .map(new Function<List<Artist>, List<Artist>>() {
                    @Override
                    public List<Artist> apply(List<Artist> artists) throws Exception {
                        Collections.sort(artists, new Comparator<Artist>() {
                            @Override
                            public int compare(Artist artist, Artist t1) {
                                return artist.getName().compareTo(t1.getName());
                            }
                        });
                        return artists;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Consumer<List<Artist>>() {
                            @Override
                            public void accept(List<Artist> artists) throws Exception {
                                mView.showArtist(artists);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {

                            }
                        });
    }

}
