
package com.example.konstantin.rgand.net.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TopAlbums {

    @SerializedName("album")
    private List<Album> album;
//    @SerializedName("@attr")
//    private AlbumsAttr attr;

    public List<Album> getAlbum() {
        return album;
    }

//    public AlbumsAttr getAttr() {
//        return attr;
//    }

}
