
package com.example.konstantin.rgand.net.model;

import com.google.gson.annotations.SerializedName;

public class TopAlbumsResponse {

    @SerializedName("topalbums")
    private TopAlbums topalbums;

    public TopAlbums getTopAlbums() {
        return topalbums;
    }

}
