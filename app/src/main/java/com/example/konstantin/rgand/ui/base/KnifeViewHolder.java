package com.example.konstantin.rgand.ui.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public abstract class KnifeViewHolder<T> extends RecyclerView.ViewHolder {

    public KnifeViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public abstract void fillData(T object);

}
