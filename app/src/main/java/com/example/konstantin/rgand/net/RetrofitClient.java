package com.example.konstantin.rgand.net;

import android.content.Context;

import javax.inject.Inject;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static final String ROOT = "http://ws.audioscrobbler.com/2.0/";
    private static final int CACHE_SIZE = 50 * 1024 * 1024;

    private API mService;

    @Inject
    public RetrofitClient(Context context) {
        setupClient(context);
    }

    private void setupClient(Context context) {
        final Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(ROOT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getClient(context))
                .build();
        mService = restAdapter.create(API.class);
    }

    public API getService() {
        return mService;
    }

    private OkHttpClient getClient(Context context) {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .cache(new Cache(context.getCacheDir(), CACHE_SIZE))
                .build();
    }

}
