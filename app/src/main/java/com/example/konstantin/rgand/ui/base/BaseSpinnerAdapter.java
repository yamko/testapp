package com.example.konstantin.rgand.ui.base;

import android.content.Context;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.widget.SpinnerAdapter;

import java.util.ArrayList;
import java.util.List;


public abstract class BaseSpinnerAdapter<T> implements SpinnerAdapter {

    protected final List<T> mItems;
    protected final LayoutInflater mLayoutInflater;
    private final DataSetObservable mDataSetObservable;

    public BaseSpinnerAdapter(Context context) {
        mLayoutInflater = LayoutInflater.from(context);
        mItems = new ArrayList<>();
        mDataSetObservable = new DataSetObservable();
    }

    public void setItems(@Nullable List<T> items) {
        mItems.clear();
        if (items == null) {
            notifyDataSetInvalidated();
            return;
        }
        mItems.addAll(items);
        mDataSetObservable.notifyChanged();
    }

    public List<T> getItems() {
        return mItems;
    }

    public void notifyDataSetChanged() {
        mDataSetObservable.notifyChanged();
    }

    public void notifyDataSetInvalidated() {
        mDataSetObservable.notifyInvalidated();
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        mDataSetObservable.registerObserver(observer);
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        mDataSetObservable.unregisterObserver(observer);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public T getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return mItems.isEmpty();
    }
}
