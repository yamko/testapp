
package com.example.konstantin.rgand.net.model;

import com.google.gson.annotations.SerializedName;

public class TopArtistsResponse {

    @SerializedName("topartists")
    private TopArtists topartists;

    public TopArtists getTopArtists() {
        return topartists;
    }

}
