package com.example.konstantin.rgand.di;

import com.example.konstantin.rgand.data.db.entity.ArtistDb;
import com.example.konstantin.rgand.data.db.entity.ArtistImageDb;
import com.example.konstantin.rgand.data.db.utils.ArtistImagesMapper;
import com.example.konstantin.rgand.data.db.utils.ArtistsMapper;
import com.example.konstantin.rgand.data.db.utils.ModelMapper;
import com.example.konstantin.rgand.data.repositories.artist.ArtistLocalStorage;
import com.example.konstantin.rgand.data.repositories.artist.ArtistRemoteStorage;
import com.example.konstantin.rgand.data.repositories.artist.ArtistsRepository;
import com.example.konstantin.rgand.data.repositories.artist.ArtistsRepositoryImpl;
import com.example.konstantin.rgand.data.db.RoomDb;
import com.example.konstantin.rgand.di.qualifier.Local;
import com.example.konstantin.rgand.di.qualifier.Remote;
import com.example.konstantin.rgand.di.qualifier.Repository;
import com.example.konstantin.rgand.net.RetrofitClient;
import com.example.konstantin.rgand.net.model.Artist;
import com.example.konstantin.rgand.net.model.Image;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ArtistRepoModule {

    @Singleton
    @Provides
    @Remote
    ArtistsRepository provideArtistsRemoteStorage(RetrofitClient retrofitClient) {
        return new ArtistRemoteStorage(retrofitClient);
    }

    @Singleton
    @Provides
    @Local
    ArtistsRepository provideArtistsLocalStorage(RoomDb roomDb, ModelMapper<Image, ArtistImageDb> artistImageMapper,
                                                 ModelMapper<Artist, ArtistDb> artistMapper) {
        return new ArtistLocalStorage(roomDb, artistImageMapper, artistMapper);
    }

    @Singleton
    @Repository
    @Provides
    ArtistsRepository provideArtistsRepository(@Remote ArtistsRepository remote, @Local ArtistsRepository local) {
        return new ArtistsRepositoryImpl(remote, local);
    }

    @Singleton
    @Provides
    ModelMapper<Image, ArtistImageDb> provideArtistsImageMapper() {
        return new ArtistImagesMapper();
    }

    @Singleton
    @Provides
    ModelMapper<Artist, ArtistDb> provideArtistsMapper() {
        return new ArtistsMapper();
    }

}
