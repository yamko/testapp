package com.example.konstantin.rgand.ui.base;

import io.reactivex.annotations.NonNull;

public abstract class BasePresenter<V extends BaseView> {

    protected V mView;

    public void bindView(@NonNull V view){
        mView = view;
    }

    public void unbindView(){
        mView = null;
    }

}
