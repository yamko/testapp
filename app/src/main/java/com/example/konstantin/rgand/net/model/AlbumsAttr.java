
package com.example.konstantin.rgand.net.model;

import com.google.gson.annotations.SerializedName;

public class AlbumsAttr {

    @SerializedName("artist")
    private String artist;
    @SerializedName("page")
    private String page;
    @SerializedName("perPage")
    private String perPage;
    @SerializedName("totalPages")
    private String totalPages;
    @SerializedName("total")
    private String total;

    public String getArtist() {
        return artist;
    }

    public String getPage() {
        return page;
    }

    public String getPerPage() {
        return perPage;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public String getTotal() {
        return total;
    }

}
