
package com.example.konstantin.rgand.net.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Artist {

    @SerializedName("name")
    private String name;
    @SerializedName("listeners")
    private String listeners;
    @SerializedName("mbid")
    private String mbid;
    @SerializedName("url")
    private String url;
    @SerializedName("streamable")
    private String streamable;
    @SerializedName("image")
    private List<Image> image = null;

    public String getName() {
        return name;
    }

    public String getListeners() {
        return listeners;
    }

    public String getMbid() {
        return mbid;
    }

    public String getUrl() {
        return url;
    }

    public String getStreamable() {
        return streamable;
    }

    public List<Image> getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setListeners(String listeners) {
        this.listeners = listeners;
    }

    public void setMbid(String mbid) {
        this.mbid = mbid;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setStreamable(String streamable) {
        this.streamable = streamable;
    }

    public void setImage(List<Image> image) {
        this.image = image;
    }
}
