package com.example.konstantin.rgand.di;

import com.example.konstantin.rgand.data.db.entity.AlbumDb;
import com.example.konstantin.rgand.data.db.entity.AlbumImageDb;
import com.example.konstantin.rgand.data.db.entity.ArtistDb;
import com.example.konstantin.rgand.data.db.entity.ArtistImageDb;
import com.example.konstantin.rgand.data.db.utils.AlbumImagesMapper;
import com.example.konstantin.rgand.data.db.utils.AlbumsMapper;
import com.example.konstantin.rgand.data.db.utils.ArtistImagesMapper;
import com.example.konstantin.rgand.data.db.utils.ArtistsMapper;
import com.example.konstantin.rgand.data.db.utils.ModelMapper;
import com.example.konstantin.rgand.data.repositories.album.AlbumLocalStorage;
import com.example.konstantin.rgand.data.repositories.album.AlbumRemoteStorage;
import com.example.konstantin.rgand.data.repositories.album.AlbumRepository;
import com.example.konstantin.rgand.data.repositories.album.AlbumsRepositoryImpl;
import com.example.konstantin.rgand.data.db.RoomDb;
import com.example.konstantin.rgand.di.qualifier.Local;
import com.example.konstantin.rgand.di.qualifier.Remote;
import com.example.konstantin.rgand.di.qualifier.Repository;
import com.example.konstantin.rgand.net.RetrofitClient;
import com.example.konstantin.rgand.net.model.Album;
import com.example.konstantin.rgand.net.model.Artist;
import com.example.konstantin.rgand.net.model.Image;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AlbumRepoModule {

    @Singleton
    @Provides
    @Remote
    AlbumRepository provideAlbumsRemoteStorage(RetrofitClient retrofitClient) {
        return new AlbumRemoteStorage(retrofitClient);
    }

    @Singleton
    @Provides
    @Repository
    AlbumRepository provideAlbumsRepository(@Remote AlbumRepository remote, @Local AlbumRepository local) {
        return new AlbumsRepositoryImpl(remote, local);
    }

    @Singleton
    @Provides
    @Local
    AlbumRepository provideAlbumsLocalStorage(RoomDb roomDb, ModelMapper<Image, AlbumImageDb> albumImageMapper,
                                              ModelMapper<Album, AlbumDb> albumMapper) {
        return new AlbumLocalStorage(roomDb, albumImageMapper, albumMapper);
    }

    @Singleton
    @Provides
    ModelMapper<Image, AlbumImageDb> provideAlbumImageMapper() {
        return new AlbumImagesMapper();
    }

    @Singleton
    @Provides
    ModelMapper<Album, AlbumDb> provideAlbumMapper() {
        return new AlbumsMapper();
    }

}
