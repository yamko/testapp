package com.example.konstantin.rgand.data.db.utils;

public interface ModelMapper<T, M> {

    M mapIntoDbModel(T item);

    T mapIntoLogicModel(M item);

}
