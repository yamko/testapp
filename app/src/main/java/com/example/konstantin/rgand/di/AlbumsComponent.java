package com.example.konstantin.rgand.di;

import com.example.konstantin.rgand.di.scope.PerActivity;
import com.example.konstantin.rgand.ui.artist.ArtistActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = AlbumsModule.class)
public interface AlbumsComponent {

    void inject(ArtistActivity albumsActivity);

}
