package com.example.konstantin.rgand.di;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ArtistRepoModule.class, AlbumRepoModule.class})
public interface AppComponent {

    OpeningComponent provideOpeningComponent(OpeningModule openingModule);

    AlbumsComponent provideAlbumsComponent(AlbumsModule albumsModule);

}
