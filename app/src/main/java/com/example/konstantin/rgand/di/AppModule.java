package com.example.konstantin.rgand.di;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.konstantin.rgand.data.repositories.country.CountriesRepository;
import com.example.konstantin.rgand.data.repositories.country.CountriesRepositoryImlp;
import com.example.konstantin.rgand.data.db.RoomDb;
import com.example.konstantin.rgand.net.RetrofitClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final Application mApplication;

    public AppModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    @Singleton
    @Provides
    Context provideContext() {
        return mApplication;
    }

    @Singleton
    @Provides
    RetrofitClient provideRetrofitClient(Context context) {
        return new RetrofitClient(context);
    }

    @Singleton
    @Provides
    RoomDb provideDatabase(Context context){
        return Room.databaseBuilder(context, RoomDb.class, RoomDb.DB_NAME).build();
    }

    @Singleton
    @Provides
    CountriesRepository provideCountriesRepository() {
        return new CountriesRepositoryImlp();
    }

}
