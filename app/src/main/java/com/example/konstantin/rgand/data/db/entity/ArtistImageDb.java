package com.example.konstantin.rgand.data.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity(foreignKeys = @ForeignKey(entity = ArtistDb.class,
        parentColumns = "artist_id", childColumns = "artist_id",
        onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey.CASCADE))
public class ArtistImageDb {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long mId;

    @ColumnInfo(name = "size")
    private String mSize;

    @ColumnInfo(name = "url")
    private String mUrl;

    @ColumnInfo(name = "artist_id")
    private String mArtistId;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getSize() {
        return mSize;
    }

    public void setSize(String size) {
        mSize = size;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getArtistId() {
        return mArtistId;
    }

    public void setArtistId(String artistId) {
        mArtistId = artistId;
    }
}
