package com.example.konstantin.rgand.data.repositories.album;

import com.example.konstantin.rgand.net.model.Album;

import java.util.List;

import io.reactivex.Single;

public interface AlbumRepository {

    void saveArtistAlbums(List<Album> albums);

    Single<List<Album>> getArtistsAlbums(String artist);

}
