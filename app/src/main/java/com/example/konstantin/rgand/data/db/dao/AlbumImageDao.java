package com.example.konstantin.rgand.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.konstantin.rgand.data.db.entity.AlbumImageDb;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface AlbumImageDao {

    @Query("SELECT * FROM AlbumImageDb WHERE album_id = :albumId")
    List<AlbumImageDb> getAllImagesForAlbum(String albumId);

    @Query("SELECT * FROM AlbumImageDb WHERE album_id IN (:albumIds)")
    Single<List<AlbumImageDb>> getAllImagesForAlbums(List<String> albumIds);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void saveAlbumImages(List<AlbumImageDb> albumImageDbs);

}
