package com.example.konstantin.rgand.data.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class ArtistDb {

    @PrimaryKey
    @ColumnInfo(name = "artist_id")
    private String mId;

    @ColumnInfo(name = "artist_name")
    private String mName;

    @ColumnInfo(name = "plays_count")
    private String mPlaysNumber;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPlaysNumber() {
        return mPlaysNumber;
    }

    public void setPlaysNumber(String playsNumber) {
        mPlaysNumber = playsNumber;
    }
}
