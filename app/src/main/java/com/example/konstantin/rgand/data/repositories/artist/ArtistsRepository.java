package com.example.konstantin.rgand.data.repositories.artist;


import com.example.konstantin.rgand.net.model.Artist;

import java.util.List;

import io.reactivex.Single;

public interface ArtistsRepository {

    void saveArtists(List<Artist> artists, String country);

    Single<List<Artist>> getArtists(String country);

}
