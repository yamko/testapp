package com.example.konstantin.rgand.ui.base;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerAdapter<ITEM_TYPE> extends RecyclerView.Adapter<KnifeViewHolder<ITEM_TYPE>> {

    protected final List<ITEM_TYPE> mItems;
    protected final LayoutInflater mLayoutInflater;

    public BaseRecyclerAdapter(Context context) {
        mLayoutInflater = LayoutInflater.from(context);
        mItems = new ArrayList<>();
    }

    public void setItems(@Nullable List<ITEM_TYPE> items) {
        mItems.clear();
        if (items == null) {
            return;
        }
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    public void invalidateItems() {
        mItems.clear();
        notifyDataSetChanged();
    }

    public List<ITEM_TYPE> getItems() {
        return mItems;
    }

    @Override
    public void onBindViewHolder(KnifeViewHolder<ITEM_TYPE> holder, int position) {
        holder.fillData(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}
