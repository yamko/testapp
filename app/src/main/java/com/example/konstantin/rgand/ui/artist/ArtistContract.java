package com.example.konstantin.rgand.ui.artist;

import com.example.konstantin.rgand.net.model.Album;
import com.example.konstantin.rgand.ui.base.BasePresenter;
import com.example.konstantin.rgand.ui.base.BaseView;

import java.util.List;

public interface ArtistContract {

    abstract class ArtistPresenter extends BasePresenter<ArtistView> {

        abstract void loadAlbums(String artistName);

    }

    interface ArtistView extends BaseView {

        void showAlbums(List<Album> albums);

        void loadArtistImage(String url);

    }

}
