package com.example.konstantin.rgand.data.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class AlbumDb {

    @PrimaryKey
    @ColumnInfo(name = "album_id")
    private String mId;

    @ColumnInfo(name = "album_name")
    private String mAlbumName;

    @ColumnInfo(name = "plays_number")
    private int mPlaysNumber;

    @ColumnInfo(name = "artist_id")
    private String mArtistId;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getAlbumName() {
        return mAlbumName;
    }

    public void setAlbumName(String albumName) {
        mAlbumName = albumName;
    }

    public int getPlaysNumber() {
        return mPlaysNumber;
    }

    public void setPlaysNumber(int playsNumber) {
        mPlaysNumber = playsNumber;
    }

    public String getArtistId() {
        return mArtistId;
    }

    public void setArtistId(String artistId) {
        mArtistId = artistId;
    }
}
