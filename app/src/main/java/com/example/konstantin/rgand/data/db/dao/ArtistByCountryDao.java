package com.example.konstantin.rgand.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.konstantin.rgand.data.db.entity.ArtistByCountryDb;
import com.example.konstantin.rgand.data.db.entity.ArtistDb;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface ArtistByCountryDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(List<ArtistByCountryDb> entityList);

}
