package com.example.konstantin.rgand.ui.opening;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.konstantin.rgand.R;
import com.example.konstantin.rgand.core.RgApp;
import com.example.konstantin.rgand.di.OpeningComponent;
import com.example.konstantin.rgand.di.OpeningModule;
import com.example.konstantin.rgand.net.model.Artist;
import com.example.konstantin.rgand.ui.artist.ArtistActivity;
import com.example.konstantin.rgand.ui.base.BaseActivity;
import com.example.konstantin.rgand.utils.ImageUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class OpeningActivity extends BaseActivity implements OpeningContract.OpeningView,
        AdapterView.OnItemSelectedListener {

    @BindView(R.id.opening_rv) RecyclerView mArtistList;
    @BindView(R.id.toolbar_country) Spinner mCountriesSpinner;

    @Inject OpeningContract.OpeningPresenter mOpeningPresenter;

    private OpeningComponent mOpeningComponent;
    private ArtistAdapter mOpeningArtistAdapter;
    private CountriesAdapter mCountriesAdapter;

    @LayoutRes
    @Override
    protected int getContentView() {
        return R.layout.activity_opening;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mOpeningComponent = ((RgApp) getApplication())
                .getAppComponent()
                .provideOpeningComponent(new OpeningModule());

        mOpeningComponent.inject(this);
        mOpeningArtistAdapter = new ArtistAdapter(this, new OnArtistItemClickListener());
        mArtistList.setAdapter(mOpeningArtistAdapter);
        mArtistList.setLayoutManager(new LinearLayoutManager(this));

        mCountriesAdapter = new CountriesAdapter(this);
        mCountriesSpinner.setAdapter(mCountriesAdapter);
        mCountriesSpinner.setOnItemSelectedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mOpeningPresenter.bindView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mOpeningPresenter.unbindView();
    }

    @Override
    public void showArtist(List<Artist> artists) {
        mOpeningArtistAdapter.setItems(artists);
    }

    @Override
    public void showError(String message) {
    }

    @Override
    public void showLoadedCountries(List<String> countries) {
        mCountriesAdapter.setItems(countries);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mOpeningPresenter.loadArtistsByCountry(mCountriesAdapter.getItem(position));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public final class OnArtistItemClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final int position = mArtistList.getChildAdapterPosition(v);
            final ImageView artistImage = ((ArtistAdapter.ArtistVH) mArtistList.getChildViewHolder(v)).mArtistImage;
            final Artist artist = mOpeningArtistAdapter.getItems().get(position);
            final String imageUrl = ImageUtils.getImageUrl(ImageUtils.EXTRA_LARGE, artist.getImage());
            ArtistActivity.start(OpeningActivity.this, artist.getName(), imageUrl,
                    artistImage, artist.getMbid());
        }
    }


}
