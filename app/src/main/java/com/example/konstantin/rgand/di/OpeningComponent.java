package com.example.konstantin.rgand.di;

import com.example.konstantin.rgand.di.scope.PerActivity;
import com.example.konstantin.rgand.ui.opening.OpeningActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = OpeningModule.class)
public interface OpeningComponent {

    void inject(OpeningActivity activity);

}
