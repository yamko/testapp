package com.example.konstantin.rgand.ui.opening;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.konstantin.rgand.R;
import com.example.konstantin.rgand.net.model.Artist;
import com.example.konstantin.rgand.ui.base.BaseRecyclerAdapter;
import com.example.konstantin.rgand.ui.base.KnifeViewHolder;
import com.example.konstantin.rgand.utils.ImageUtils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import butterknife.BindView;

public class ArtistAdapter extends BaseRecyclerAdapter<Artist> {

    private final View.OnClickListener mOnClickListener;

    public ArtistAdapter(Context context, View.OnClickListener onClickListener) {
        super(context);
        mOnClickListener = onClickListener;
    }

    @Override
    public KnifeViewHolder<Artist> onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = mLayoutInflater.inflate(R.layout.item_artis, parent, false);
        view.setOnClickListener(mOnClickListener);
        return new ArtistVH(view);
    }

    static final class ArtistVH extends KnifeViewHolder<Artist> {

        @BindView(R.id.item_artist_image)
        ImageView mArtistImage;
        @BindView(R.id.item_artist_name)
        TextView mArtistName;
        @BindView(R.id.item_artist_listeners)
        TextView mArtistListeners;

        private final DecimalFormat mDecimalFormat;

        public ArtistVH(View itemView) {
            super(itemView);
            mDecimalFormat =(DecimalFormat) NumberFormat.getInstance(Locale.US);
            final DecimalFormatSymbols symbols = mDecimalFormat.getDecimalFormatSymbols();
            symbols.setGroupingSeparator(' ');
            mDecimalFormat.setDecimalFormatSymbols(symbols);
        }

        @Override
        public void fillData(Artist object) {
            ImageUtils.loadImage(itemView.getContext(),
                    ImageUtils.getImageUrl(ImageUtils.LARGE, object.getImage()), mArtistImage);
            mArtistName.setText(object.getName());
            mArtistListeners.setText(mDecimalFormat.format(Integer.valueOf(object.getListeners())));
        }
    }
}