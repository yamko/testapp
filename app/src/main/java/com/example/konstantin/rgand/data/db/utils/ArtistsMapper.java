package com.example.konstantin.rgand.data.db.utils;

import com.example.konstantin.rgand.data.db.entity.ArtistDb;
import com.example.konstantin.rgand.net.model.Artist;

public class ArtistsMapper implements ModelMapper<Artist,ArtistDb> {

    @Override
    public ArtistDb mapIntoDbModel(Artist item) {
        final ArtistDb artistDb = new ArtistDb();
        artistDb.setName(item.getName());
        artistDb.setId(item.getMbid());
        artistDb.setPlaysNumber(item.getListeners());
        return artistDb;
    }

    @Override
    public Artist mapIntoLogicModel(ArtistDb item) {
        final Artist artist = new Artist();
        artist.setMbid(item.getId());
        artist.setName(item.getName());
        artist.setListeners(item.getPlaysNumber());
        return artist;
    }
}
