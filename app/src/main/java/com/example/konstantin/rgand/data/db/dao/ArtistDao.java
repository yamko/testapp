package com.example.konstantin.rgand.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.konstantin.rgand.data.db.entity.ArtistDb;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface ArtistDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertArtist(List<ArtistDb> entities);

    @Query("SELECT * FROM ArtistDb WHERE artist_id IN (SELECT artist_id FROM ArtistByCountryDb WHERE country_name = :country)")
    Single<List<ArtistDb>> getArtistsByCountry(String country);

}
