package com.example.konstantin.rgand.ui.opening;

import com.example.konstantin.rgand.net.model.Artist;
import com.example.konstantin.rgand.ui.base.BasePresenter;
import com.example.konstantin.rgand.ui.base.BaseView;

import java.util.List;

public interface OpeningContract {

    abstract class OpeningPresenter extends BasePresenter<OpeningView> {

        public abstract void loadArtistsByCountry(String country);

    }

    interface OpeningView extends BaseView {

        void showArtist(List<Artist> artists);

        void showError(String message);

        void showLoadedCountries(List<String> countries);

    }

}
