package com.example.konstantin.rgand.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.konstantin.rgand.data.db.dao.AlbumDao;
import com.example.konstantin.rgand.data.db.dao.AlbumImageDao;
import com.example.konstantin.rgand.data.db.dao.ArtistDao;
import com.example.konstantin.rgand.data.db.dao.ArtistByCountryDao;
import com.example.konstantin.rgand.data.db.dao.ArtistImageDao;
import com.example.konstantin.rgand.data.db.entity.AlbumDb;
import com.example.konstantin.rgand.data.db.entity.AlbumImageDb;
import com.example.konstantin.rgand.data.db.entity.ArtistDb;
import com.example.konstantin.rgand.data.db.entity.ArtistImageDb;
import com.example.konstantin.rgand.data.db.entity.ArtistByCountryDb;

@Database(version = 1, entities = {ArtistDb.class, AlbumDb.class, ArtistByCountryDb.class,
        ArtistImageDb.class, AlbumImageDb.class})
public abstract class RoomDb extends RoomDatabase {

    public static final String DB_NAME = "app_db";

    public abstract ArtistDao artistDao();

    public abstract AlbumDao albumDao();

    public abstract ArtistByCountryDao artistInCountryDao();

    public abstract AlbumImageDao albumImageDao();

    public abstract ArtistImageDao artistImageDao();

}
