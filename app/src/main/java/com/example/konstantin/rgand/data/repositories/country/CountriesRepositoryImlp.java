package com.example.konstantin.rgand.data.repositories.country;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;

public class CountriesRepositoryImlp implements CountriesRepository {

    @Override
    public Observable<List<String>> loadCountries() {
        final List<String> list = Arrays.asList("Ukraine", "Germany", "Spain");
        return Observable.just(list);
    }
}
