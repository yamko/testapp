package com.example.konstantin.rgand.data.db.utils;

import com.example.konstantin.rgand.data.db.entity.AlbumDb;
import com.example.konstantin.rgand.data.db.entity.AlbumImageDb;

import java.util.List;

public class AlbumTransferModel {

    private AlbumDb mAlbumDb;

    private List<AlbumImageDb> mAlbumImageDbs;

    public AlbumDb getAlbumDb() {
        return mAlbumDb;
    }

    public void setAlbumDb(AlbumDb albumDb) {
        mAlbumDb = albumDb;
    }

    public List<AlbumImageDb> getAlbumImageDbs() {
        return mAlbumImageDbs;
    }

    public void setAlbumImageDbs(List<AlbumImageDb> albumImageDbs) {
        mAlbumImageDbs = albumImageDbs;
    }
}
