package com.example.konstantin.rgand.di;

import com.example.konstantin.rgand.data.repositories.artist.ArtistsRepository;
import com.example.konstantin.rgand.data.repositories.country.CountriesRepository;
import com.example.konstantin.rgand.di.qualifier.Repository;
import com.example.konstantin.rgand.di.scope.PerActivity;
import com.example.konstantin.rgand.ui.opening.OpeningContract;
import com.example.konstantin.rgand.ui.opening.OpeningPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class OpeningModule {

//    @PerActivity
//    @Provides
//    ArtistAdapter provideArtistAdapter(Context context){
//        return new ArtistAdapter(context);
//    }

    @PerActivity
    @Provides
    OpeningContract.OpeningPresenter providePresenter(@Repository ArtistsRepository artistsRepository,
                                                      CountriesRepository countriesRepository){
        return new OpeningPresenterImpl(artistsRepository, countriesRepository);
    }

}
