package com.example.konstantin.rgand.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.konstantin.rgand.data.db.entity.AlbumImageDb;
import com.example.konstantin.rgand.data.db.entity.ArtistImageDb;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface ArtistImageDao {

    @Query("SELECT * FROM ArtistImageDb WHERE artist_id = :artistId")
    List<ArtistImageDb> getAllImagesForAtrist(String artistId);

    @Query("SELECT * FROM ArtistImageDb WHERE artist_id IN (:albumIds)")
    Single<List<AlbumImageDb>> getAllImagesForAlbums(List<String> albumIds);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void saveArtistsImages(List<ArtistImageDb> artistImageDbs);

}
