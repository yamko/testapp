package com.example.konstantin.rgand.data.db.utils;

import com.example.konstantin.rgand.data.db.entity.ArtistImageDb;
import com.example.konstantin.rgand.net.model.Image;

public class ArtistImagesMapper implements ModelMapper<Image, ArtistImageDb> {

    @Override
    public ArtistImageDb mapIntoDbModel(Image item) {
        final ArtistImageDb imageDb = new ArtistImageDb();
        imageDb.setSize(item.getSize());
        imageDb.setUrl(item.getText());
        return imageDb;
    }

    @Override
    public Image mapIntoLogicModel(ArtistImageDb item) {
        final Image image = new Image();
        image.setSize(item.getSize());
        image.setText(item.getUrl());
        return image;
    }
}
