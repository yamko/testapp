package com.example.konstantin.rgand.utils;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.konstantin.rgand.net.model.Image;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public class ImageUtils {

    public static final String EXTRA_LARGE = "extralarge";
    public static final String LARGE = "large";
    public static final String MEDIUM = "medium";
    public static final String SMALL = "small";

    @StringDef({EXTRA_LARGE, LARGE, MEDIUM, SMALL})
    @Retention(RetentionPolicy.RUNTIME)
    @interface Size {
    }

    private ImageUtils(){
    }

    public static void loadImage(Context context, @Nullable String url, ImageView imageView){
        if(TextUtils.isEmpty(url)){
            return;
        }
        Glide.with(context).asDrawable().load(url).into(imageView);
    }

    @Nullable
    public static String getImageUrl(@Size String size, List<Image> images){
        for (Image image : images) {
            if (image.getSize().equals(size)) {
                return image.getText();
            }
        }
        return null;
    }

}
