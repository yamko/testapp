package com.example.konstantin.rgand.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.konstantin.rgand.data.db.entity.AlbumDb;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface AlbumDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertArtistAlbums(List<AlbumDb> list);

    @Query("SELECT * FROM AlbumDb WHERE artist_id = :artistId")
    Single<List<AlbumDb>> getAllArtistsAlbums(String artistId);

}
