package com.example.konstantin.rgand.data.repositories.artist;

import com.example.konstantin.rgand.net.model.Artist;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;

public class ArtistsRepositoryImpl implements ArtistsRepository {

    private ArtistsRepository mLocal;
    private ArtistsRepository mRemote;

    @Inject
    public ArtistsRepositoryImpl(ArtistsRepository remote, ArtistsRepository local) {
        mRemote = remote;
        mLocal = local;
    }

    @Override
    public void saveArtists(List<Artist> artists, String country) {

    }

    @Override
    public Single<List<Artist>> getArtists(final String country) {
        return mLocal.getArtists(country)
                .onErrorResumeNext(new Function<Throwable, SingleSource<? extends List<Artist>>>() {
                    @Override
                    public SingleSource<? extends List<Artist>> apply(Throwable throwable) throws Exception {
                        return mRemote.getArtists(country)
                                .map(new Function<List<Artist>, List<Artist>>() {
                                    @Override
                                    public List<Artist> apply(List<Artist> artists) throws Exception {
                                        mLocal.saveArtists(artists, country);
                                        return artists;
                                    }
                                });
                    }
                });
    }
}
