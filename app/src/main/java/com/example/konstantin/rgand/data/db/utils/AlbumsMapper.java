package com.example.konstantin.rgand.data.db.utils;

import com.example.konstantin.rgand.data.db.entity.AlbumDb;
import com.example.konstantin.rgand.net.model.Album;

public class AlbumsMapper implements ModelMapper<Album, AlbumDb> {

    public AlbumsMapper() {
    }

    @Override
    public AlbumDb mapIntoDbModel(Album item) {
        final AlbumDb albumDb = new AlbumDb();
        albumDb.setAlbumName(item.getName());
        albumDb.setId(item.getMbid());
        albumDb.setPlaysNumber(item.getPlaycount());
        albumDb.setArtistId(item.getArtist().getName());
        return albumDb;
    }

    @Override
    public Album mapIntoLogicModel(AlbumDb item) {
        final Album album = new Album();
        album.setName(item.getAlbumName());
        album.setPlaycount(item.getPlaysNumber());
        album.setMbid(item.getId());
        return album;
    }
}
